from django.apps import AppConfig


class TarlyConfig(AppConfig):
    name = 'Tarly'
