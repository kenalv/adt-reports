from rest_framework import serializers

from Lannister.models import Contactos


class ContactosSerializer(serializers.Serializer):
    contact_id = serializers.CharField(max_length=150)
    name = serializers.CharField(max_length=150)
    lastname = serializers.CharField(max_length=150)

    def update(self, instance, validated_data):
        instance.contact_id = validated_data.get('contact_id', instance.contact_id)
        instance.name = validated_data.get('name', instance.name)
        instance.lastname = validated_data.get('lastname', instance.lastname)
        return instance.save()

    def create(self, validated_data):
        tmpObject = Contactos.objects.filter(contact_id = validated_data['contact_id'])
        if not tmpObject:
            return Contactos.objects.create(**validated_data)

        self.update(instance=tmpObject.first(),validated_data = validated_data)
        return Contactos(**validated_data)


