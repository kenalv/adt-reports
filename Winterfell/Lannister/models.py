from django.db import models

# Create your models here.

class Contactos(models.Model):
    contact_id = models.CharField(max_length=150)
    name = models.CharField(max_length=150)
    lastname = models.CharField(max_length=150)

    def __str__(self):
        return f'{self.name}'
