#!/usr/bin/python
# encoding=utf8
import pymongo
from pymongo import MongoClient
import datetime
from datetime import datetime
from bson.objectid import ObjectId
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def __init():
    try:
        checkTimeDifference()
    except Exception as e:
        print(str(e))


def checkTimeDifference():  # This function checks the time difference of Sansay files from now, if that difference is one hour or more, sends an email with that file time, else only print in console a message to say accepted
    try:
        currentDateTime = datetime.now()

        # Sonus
        # archiveNameList = getLastInsertedArchiveName(FALSE)
        # archiveName = archiveNameList[0]['ArchiveName']
        # date = archiveName.split(".")[1]
        # lastInsertedArchiveDateTime = datetime.strptime(date, '%Y%m%d%H%M%S')

        # diff = currentDateTime - lastInsertedArchiveDateTime
        #
        # minDiff = diff.total_seconds()/60
        #
        # if minDiff > 59:
        # 	#send email
        # 	print "sending email sonus not inserting"
        # 	print minDiff
        # 	sendEmailLI(lastInsertedArchiveDateTime, "Sonus")
        # else:
        # 	print "dentro de rango de aceptacion sonus"
        # 	print minDiff

        archiveNameListSansay = getLastInsertedArchiveName(True)  # getLastInsertedArchiveName  TRUE = SANSAY

        archiveNameSansay = archiveNameListSansay[0]['ArchiveName']
        datesansay = str(archiveNameSansay.split("-")[0]) + str(archiveNameSansay.split("-")[1])
        lastInsertedArchiveDateTimeSansay = datetime.strptime(datesansay, '%Y%m%d%H%M')

        diffSansay = currentDateTime - lastInsertedArchiveDateTimeSansay  # difference between this date from now

        minDiffSanSay = diffSansay.total_seconds() / 60  # time difference in minutes

        if minDiffSanSay > 59:  # if the last file inserttion
            # send email
            print("sending email sansay not inserting")
            print(minDiffSanSay)
            # Create the body of the message (a plain-text and an HTML version).
            text = "Warning! Please contact Luis Solano Diaz and Erick Cubero Jimenez since the CDRs are not being inserted and the fraud detection software is not working! Last inserted archive was on: %s" % (
                lastInsertedArchiveDateTimeSansay)
            html = """\
			<html>
			<head></head>
			<body>
				<p>Warning!<br>
				Please contact Luis Solano Diaz and Erick Cubero Jimenez since the CDRs are not being inserted and the fraud detection software is not working!<br>
					Last inserted archive was on: %s
				</p>
			</body>
			</html>
			""" % (lastInsertedArchiveDateTimeSansay)

            sendEmail("robot@data.cr", ["dev@data.cr"],"Warning CDRs not being inserted", text, html)
        else:
            print("dentro de rango de aceptacion sansay")
            print(minDiffSanSay)


    except Exception as e:
        raise


def connectMongo(_user, _pass, _node0, _node1, _replicaSet, _database,
                 _collection):  # This function connects to MongoDB with replicaset
    try:
        client = MongoClient("mongodb://%s:%s@%s,%s/?replicaSet=%s" % (_user, _pass, _node0, _node1, _replicaSet))
        db = client[_database]
        collection = db[_collection]

        return collection
    except Exception as e:
        sendEmail("robot@data.cr", ["dev@data.cr"], "checkCDR error", "checkCDR. Not connecting to MongoDB.", """\
		<html>
		  <head>checkCDR</head>
		  <body>
		    <p>Not connecting to MongoDB!<br>
		    </p>
		  </body>
		</html>
		""")
        print(str(e))

def getLastInsertedArchiveName(sonus_o_Sansay):
    # This function gets last inserted archive name of SONUS or SANSAY
    # from Skywalker MongoDB CDR collection, sonus_o_Sansay = FALSE => SONUS  |  sonus_o_Sansay = TRUE => SANSAY
    try:
        collection = connectMongo("admin", "D4t42012", "mongoDBReplication0:27017", "mongoDBReplication1:27017",
                                  "mongoDBReplication", "skywalker", "CDR")
        archiveName = collection.find(
            {
                "VoipDevice": {
                    "$exists": sonus_o_Sansay
                }
            },
            {
                "_id": 0,
                "ArchiveName": 1
            }
        ).sort(
            [
                ("_id", -1)
            ]
        ).limit(1)
        return list(archiveName)

    except Exception as e:
        sendEmail("robot@data.cr", ["dev@data.cr"], "checkCDR error", "checkCDR. Not able to run query.", """\
		<html>
			<head><h2>checkCDR file</h2></head>
			<body>
				<p><strong>Location:</strong> home/dev/checkCDR.py function name: getLastInsertedArchiveName()</p>
				<p><strong>Message:</strong> Not able to run query!</p>
			</body>
		</html>
		""")
        raise

'''
def sendEmailLI(_lastInsertedArchiveDateTime, _device):
	try:
		# me == my email address
		# you == recipient's email address
		me = "robot@data.cr"
		you = ["dev@data.cr"]

		# Create message container - the correct MIME type is multipart/alternative.
		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Warning CDRs from {} are not being inserted".format(_device)
		msg['From'] = me
		msg['To'] = ','.join(you)

		# Create the body of the message (a plain-text and an HTML version).
		text = "Warning! Please contact Luis Solano Diaz and Erick Cubero Jimenez since the CDRs are not being inserted and the fraud detection software is not working! Last inserted archive was on: %s"%(_lastInsertedArchiveDateTime)
		html = """\
		<html>
		  <head></head>
		  <body>
		    <p>Warning!<br>
		       Please contact Luis Solano Diaz and Erick Cubero Jimenez since the CDRs are not being inserted and the fraud detection software is not working!<br>
		        Last inserted archive was on: %s
		    </p>
		  </body>
		</html>
		""" % (_lastInsertedArchiveDateTime)

		# Record the MIME types of both parts - text/plain and text/html.
		part1 = MIMEText(text, 'plain')
		part2 = MIMEText(html, 'html')

		# Attach parts into message container.
		# According to RFC 2046, the last part of a multipart message, in this case
		# the HTML message, is best and preferred.
		msg.attach(part1)
		msg.attach(part2)

		# Send the message via local SMTP server.
		s = smtplib.SMTP('190.7.193.70')
		# sendmail function takes 3 arguments: sender's address, recipient's address
		# and message to send - here it is sent as one string.
		s.sendmail(me, you, msg.as_string())
		s.quit()

	except Exception as e:
		raise
'''
def sendEmail(_from, _to, _subject, _text, _html):  # This function sends emails in case of not insert CDRs and fraud detection failure
    try:
        # me == my email address
        # you == recipient's email address
        me = _from
        you = _to

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = _subject
        msg['From'] = me
        msg['To'] = ','.join(you)

        # Create the body of the message (a plain-text and an HTML version).
        text = _text
        html = _html

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        # Send the message via local SMTP server.
        s = smtplib.SMTP('190.7.193.70')
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        s.sendmail(me, you, msg.as_string())
        s.quit()

    except Exception as e:
        raise

__init()
