#!/usr/bin/python
# coding: utf-8

import os
import ConfigParser
import xml.etree.ElementTree as ET

from SOAP.soap import SOAP
from SQL.sql import SQL

path = os.path.dirname(os.path.abspath( __file__ ))
config = ConfigParser.ConfigParser()
config.read("{}/config.ini".format(path))

def __init__():
    try:
        listSansayTrunkgroup = getSansayTrunkgroup() #return a list of Trunks From Sansay
        listSkywalkerTrunkgroup = getSkywalkerTrunkgroup() #return lis of Trunks from SkywalkerDB
        dictSkywalkerTrunkgroup = changeListToDict('trunkGroupId', listSkywalkerTrunkgroup)
        listTrunksNotInserted = [
            trunkS['trunkId']
            for trunkS in listSansayTrunkgroup if trunkS['trunkId'] not in dictSkywalkerTrunkgroup
        ]
        print(listTrunksNotInserted)
    except Exception as e:
        raise


def getSansayTrunkgroup():
    try:
        #Sansay API Information
        username = config.get('SansayWS', 'username')
        password = config.get('SansayWS', 'password')
        wsdlURL = config.get('SansayWS', 'wsdlURL')
        #Initialize SOAP Class
        soap = SOAP(username,password,wsdlURL)
        soap.connect()
        #Gets current information from TrunkgroupID
        methodName = 'doDownloadXmlFile'
        kwargs = {'table':'resource', 'username':username, 'password':password, 'page':0}
        currentTIDInfoResponse = soap.callMethod(methodName, kwargs)
        listTrunkgroupSansay = list()
        #print currentTIDInfoResponse
        #Checks if the SOAP request was sucessful
        if currentTIDInfoResponse['retCode'] == 200:
            #Converts String to XML Object
            root = ET.fromstring(currentTIDInfoResponse['xmlfile'])
            #listResources = root.find('XBResourceList')
            print(root.tag)
            count = 1
            for value in root:
            #print "test"
                #000252
                trunkId = value.find('trunkId').text
                lenTrunkId = len(trunkId)
                if lenTrunkId < 6:
                    for x in range(0,6-lenTrunkId):
                        trunkId = '0' + trunkId
                companyName = value.find('companyName').text
                name = value.find('name').text
                #print '********************************************************'
                #print value.tag, value.attrib
                trunk = {}
                trunk['trunkId'] = trunkId
                trunk['companyName'] = companyName
                trunk['name'] = name
                listTrunkgroupSansay.append(trunk)
                #print trunkId
                #print companyName
                #print name
                #print '********************************************************'
                count = count + 1
            #print count
        return listTrunkgroupSansay
    except Exception as e:
        raise


def changeListToDict(_index, _list):
    try:
        dict = {}
        for tupl in _list:
            dict[tupl[_index]] = tupl
        return dict
    except Exception as e:
        raise

def getSkywalkerTrunkgroup():
    try:
        server = config.get('Sywalker','server')
        user = config.get('Sywalker','user')
        password = config.get('Sywalker','password')
        database = config.get('Sywalker','database')

        sq = SQL(server, user, password, database)
        sq.connect()
        list = sq.select("select trunkGroupId from trunkgroup")
        sq.close()
        return list
    except Exception as e:
        raise


__init__()
