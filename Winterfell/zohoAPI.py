#!/usr/bin/python3
# -*- coding: latin-1 -*-

import json
import requests
import sys
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

def __init__():
    try:

        # access_token = getAccessToken()
        # print(access_token)
        # if access_token is not None:
        #     api_url = "https://www.zohoapis.com/crm/v2/settings/modules"
        #     headers = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token'])}
        #     # print(headers)
        #     type = 'get'
        #     ra = requestAPI(type,api_url,headers,json)
        #     # print(ra)
        #     if ra['response'] == 'error':
        #         refreshToken()
        #         api_url = "https://www.zohoapis.com/crm/v2/settings/modules"
        #         headers = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token'])}
        #         # print(headers)
        #         type = 'get'
        #         ra = requestAPI(type,api_url,headers,json)
        #         print(ra)
        #     else:
        #         for module in ra['content']['modules']:
        #             print(module['module_name'])
        #             if module['module_name'] == 'Accounts':
        #                 print(module)
        # type = 'get'
        # api_url = 'https://www.zohoapis.com/crm/v2/Accounts'
        # header = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token'])}
        # ra = rAPI(type,api_url,header,None)
        # print(ra)
        # 'status_code': 200
        # for account in ra['content']['data']:
        #     print(account['Account_Name'])




        type = 'get'
        api_url='https://www.zohoapis.com/crm/v2/Accounts/search?word=DAT-1911'
        header = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token'])}
        ra = rAPI(type,api_url,header,None)
        print(ra['content']['data'][0])

        if ra['status_code'] == 200:
            api_url = "https://www.zohoapis.com/crm/v2/Accounts/{}".format(ra['content']['data'][0]['id'])
            type = 'put'
            ajson = {}
            data = {}
            data['Sobre_consumo'] = True
            ajson['data'] = [data]
            ajson['trigger'] = ["approval"]

            # json = {"data": [{"Website": "test"}],"trigger": ["approval"]}
            header = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token']),'Content-Type': 'application/x-www-form-urlencoded'}
            ra = rAPI(type,api_url,header,json.dumps(ajson))
            print(ra)

        else:
            print("There was a problem {}".format(ra))


        # api_url = "https://www.zohoapis.com/crm/v2/Accounts/{}".format("3413022000003156071")
        # type = 'put'
        # json = {}
        # data = {}
        # data['website'] = "test"
        # json['data'] = [data]
        # json['trigger'] = ["approval"]
        # header = {'Authorization': 'Zoho-oauthtoken {0}'.format(config['Zoho']['access_token'])}
        # json = "{'data': [{'website': 'test'}], 'trigger': ['approval']}"
        # ra = rAPI(type,api_url,header,json)
        # print(ra)

        # type = 'post'
        # api_url = "https://bwcsoftware.free.beeceptor.com/test"
        # headers = {'Content-Type': 'application/json'
        #    # 'Authorization': 'Bearer {0}'.format(api_token)
        #    }
        # json = {
        #         "data":config['Zoho']['code']
        #         }
        # res = requestAPI(type,api_url,headers,json)
        # print(res)
    except Exception as e:
        raise
    else:
        pass
    finally:
        pass

def rAPI(_type,_api_url,_headers,_json):
    try:
        if config['Zoho']['access_token'] is not None:
            ra = requestAPI(_type,_api_url,_headers,_json)
            if ra['response'] == 'error':
                print(ra)
                # {'response': 'error', 'status_code': 401, 'content': {'code': 'AUTHENTICATION_FAILURE', 'details': {}, 'message': 'Authentication failed', 'status': 'error'}}
                if ra['status_code'] == 401 and ra['content']['code'] == "INVALID_TOKEN":
                    refreshToken()
                    rAPI(_type,_api_url,_headers,_json)
            else:
                return ra
        else:
            getAccessToken()
            config.read('config.ini')
    except Exception as e:
        raise
    else:
        pass
    finally:
        pass

def refreshToken():
    try:
        print("refreshing token")
        api_url_base = config['Zoho']['api_url_base']
        api_url = '{0}oauth/v2/token?refresh_token={1}&client_id={2}&client_secret={3}&grant_type=refresh_token'.format(api_url_base,config['Zoho']['refresh_token'],config['Zoho']['client_id'],config['Zoho']['client_secret'])
        type = 'post'
        ra = requestAPI(type,api_url,None,None)
        # print(ra)
        config['Zoho']['access_token'] = ra['content']['access_token']
        with open('config.ini', 'w') as configfile:
            config.write(configfile)
            config.read('config.ini')
    except Exception as e:
        raise
    else:
        pass
    finally:
        pass

def getAccessToken():
    try:
        if config['Zoho']['access_token'] == "":
            print("No acess token")
            api_url_base = config['Zoho']['api_url_base']
            headers = {'Content-Type': 'application/json'
               # 'Authorization': 'Bearer {0}'.format(api_token)
               }
            api_url = '{0}oauth/v2/token'.format(api_url_base)
            type = 'post'
            json = {
                    "grant_type":config['Zoho']['grant_type'],
                    "client_id":config['Zoho']['client_id'],
                    "client_secret":config['Zoho']['client_secret'],
                    "redirect_uri":config['Zoho']['redirect_uri'],
                    "code":config['Zoho']['code']
                    }
            # print(type,api_url,headers,json)
            # print(json)
            # sys.exit()
            ra = requestAPI(type,api_url,None,json)
            # print(ra)
            if "error" in ra['content']:
                print(ra['content']["error"])
            else:
                config['Zoho']['access_token'] = ra['content']['access_token']
                config['Zoho']['refresh_token'] = ra['content']['refresh_token']
                config['Zoho']['expires_in_sec'] = str(ra['content']['expires_in_sec'])
                config['Zoho']['api_domain'] = ra['content']['api_domain']
                config['Zoho']['token_type'] = ra['content']['token_type']
                with open('config.ini', 'w') as configfile:
                    config.write(configfile)
                    config.read('config.ini')
        else:
            return config['Zoho']['access_token']
    except Exception as e:
        raise
    else:
        pass
    finally:
        pass

def requestAPI(_type,_api_url_base,_headers,_json={}):
    try:
        print(_type,_api_url_base,_headers,_json)
        if _type == 'post':
            print('its a post')
            if _headers == None:
                response = requests.post(_api_url_base, data=_json)
            else:
                response = requests.post(_api_url_base, headers=_headers, data=_json)
        elif _type == 'put':
            print('its a put')
            if _headers == None:
                response = requests.put(_api_url_base, data=_json)
            else:
                print('with headers')
                response = requests.put(_api_url_base, headers=_headers, data=_json)
        else:
            print('its a get')
            response = requests.get(_api_url_base, headers=_headers)

        res = {}

        if response.status_code == 200:
            res['response'] = 'sucessfull'
            res['status_code'] = response.status_code
            res['content'] = json.loads(response.content.decode('utf-8'))
            return res
        else:
            res['response'] = 'error'
            res['status_code'] = response.status_code
            res['content'] = json.loads(response.content.decode('utf-8'))
            return res
    except Exception as e:
        raise
    else:
        pass
    finally:
        pass

__init__()
